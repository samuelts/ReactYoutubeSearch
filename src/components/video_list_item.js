import React from 'react';

const VideoListItem = ({video, onVideoSelect}) => {
  const title = video.snippet.title;
  const channelTitle = video.snippet.channelTitle;
  const thumbnailURL = video.snippet.thumbnails.default.url;

  return (
    <li onClick={() => onVideoSelect(video)} className='list-group-item'>
      <div className='video-list media'>

        <div className='media-left'>
          <img className='media-object' src={thumbnailURL}/>
        </div>

        <div className='media-body'>
          <div className='media-heading'>
            {title}
          </div>
          <div className='media-author'>
            {channelTitle}
          </div>
        </div>

      </div>
    </li>
  );
};

export default VideoListItem;