import React from 'react';

const VideoDetail = ({video}) => {
  if (!video) {
    return <div>Loading...</div>
  }

  const videoId = video.id.videoId;
  const url = `https://www.youtube.com/embed/${videoId}`;
  const link = `https://www.youtube.com/watch?v=${videoId}`;
  const title = video.snippet.title;
  const desc = video.snippet.description;

  return (
    <div className='video-detail col-md-8'>
      <div className='iframe-wrap embed-responsive embed-responsive-16by9'>
        <iframe
          className='embed-responsive-item'
          allowfullscreen="allowfullscreen"
          mozallowfullscreen="mozallowfullscreen"
          msallowfullscreen="msallowfullscreen"
          oallowfullscreen="oallowfullscreen"
          webkitallowfullscreen="webkitallowfullscreen"
          src={url}></iframe>
      </div>
      <div className='details'>
        <div className='title'><a href={link}>{title}</a></div>
        <div className='desc'>{desc}</div>
      </div>
    </div>
  );

};

export default VideoDetail;