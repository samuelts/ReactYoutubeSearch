# React YouTube Search
This is a simple app for searching YouTube videos and displaying the top five results for viewing.

Demo here: https://samuelts.com/ReactYoutubeSearch

### Screenshot

![Mobile Screencap](https://raw.githubusercontent.com/safwyls/logos/master/reactjs_youtube_player_img.png)
